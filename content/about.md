---
title: "About"
date: 2020-02-07T22:15:55+01:00

---

Hey, ich bin Max und das hier ist meine Webseite.

Ich bastle gern' an Webseiten und bin momentan als IT Projektmanager unterwegs.

Man kann mich unter [max@enten.space](mailto:max@enten.space) erreichen.

[GPG-Key](/misc/gpg.txt)
