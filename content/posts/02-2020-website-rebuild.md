---
title: "Kleiner Webseitenumbau"
date: 2020-02-07T23:04:53+0100
showDate: true
draft: false
tags: ['webseite','hugo']
---

Ich hab dieser Webseite mal einen neuen Anstrich verpasst, hat mich rund 1 Stunde gekostet.
Aber nur weil ich mich nicht für ein Theme entscheiden konnte.

Die Seite wird immer noch mit [Hugo](https://gohugo.io) gebaut und als Theme dient mir nun [Sam](https://github.com/victoriadotdev/hugo-theme-introduction), zufälligerweise ebenfalls von [Victoria Drake](https://victoria.dev/).

Ist alles ein bisschen sehr viel schlanker geworden und die einzige Veränderung die ich vornehmen musste war, dass ich die Google Schriftarten rausgeschmissen habe und entsprechend durch lokale Dateien ersetzt habe.

Momentan fehlen mir hier noch deutsche Datumsangaben und ein Knopf für einen RSS Feed.
Die kommen wahrscheinlich dann die nächsten Tage.
