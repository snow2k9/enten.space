---
title: "Webseiten auf SSL umstellen"
date: 2018-01-06T12:04:06+01:00
showDate: true
draft: false
tags: ['server','linux']
---

Wenn ihr eure Webseite ausschließlich über HTTPS erreichbar machen wollt, müsst ihr jeden HTTP-Aufruf auf HTTPS umleiten.
Die meisten Webhoster wie 1&1, Strato usw. nutzen Apache, oder eine andere möglichkeit .htaccess Dateien zu lesen, was es einem Anwender einfach macht bestimmte Änderungen der Webserver Konfiguration lokal zu überschreiben.

In die .htaccess (falls die nicht existiert, einfach erstellen) im Wurzelverzeichnis eurer Webseite tragt ihr folgendes ein:
```sh
RewriteCond %{HTTPS} off
RewriteRule ^(.*)$ https://%{HTTP_HOST}/$1 [R=301,L]
```

Falls nginx benutzt wird ohne zusätzliche Software, ist es ein bisschen komplizierter.
In eurer Konfigurationsdatei der Webseite tragt ihr folgendes ein. VORSICHT, passt den Servernamen an eure Domain an.
```sh
server {
       listen         80;
       server_name    meine.domain.de;
       return         301 https://$server_name$request_uri;
}

server {
       listen         443 ssl;
       server_name    meine.domain.de;
       # Strict-Transport-Security gegen "Man-in-the-Middle"-Attacks
       add_header Strict-Transport-Security "max-age=31536000" always;

       [...]
}
```

Mittlerweile gibt es auch den HTTP-Status 308, der auch verwendet werden könnte, statt 301
