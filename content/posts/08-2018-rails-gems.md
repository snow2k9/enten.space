---
title: "Gems für Ruby on Rails Projekte"
date: 2018-08-28T19:40:00+01:00
showDate: true
draft: false
tags: ['rails', 'programming']
---

Ich suche mir immer wieder aufs neue die Gems raus, die ich gerne als Programmierhelferlein benutzen möchte, daher hier eine kleine Zusammenfassung. Vielleicht gibt es da ja auch noch einen Gem, den ihr für nützlich halten könntet.

### [RuboCop](https://rubygems.org/gems/rubocop)
Ein Static Code Analyzer, um den Ruby Style Guide einzuhalten.
```ruby
# Gemfile
group :development do
  gem 'rubocop', require: false
end
```
Robocop kann zwischendurch etwas anstrengend sein, aber sobald man sich dran gewöhnt ist dieser Gem super praktisch. Damit RuboCop einem kein Kopfzerbrechen bereitet, gibt es noch ein paar Tipps auf dem Weg.
Man kann für die Kleinigkeiten `rubocop --auto-correct` bzw. bei einem Projekt mit Rails auch `rubocop -R --auto-correct` ausführen, dieser Befehl prüft nicht nur, sondern korrigiert auch schon sehr viel. Der `-R` Schalter prüft noch zusätzliche RailsCops.

Und meine Standardkonfiguration:
```ruby
# .rubocop.yml

#inherit_from: .rubocop_todo.yml

Metrics:
  Exclude:
    - 'config/**/*'
    - 'db/schema.rb'

Metrics/LineLength:
  Max: 130

Metrics/MethodLength:
  Max: 25

Style/Documentation:
  Enabled: false
```

Es wäre auch eine Möglichkeit RuboCop vor einem Git-Push oder einem Commit automatisch auszuführen.
Dazu muss die Datei `.git/hooks/pre-push` bzw. `.git/hooks/pre-commit` angelegt werden. Dadurch schmeißt nicht überprüfter Code im Zweifel Fehler und verhindert somit den Push/Commit

```sh
#!/usr/bin/env bash
rubocop
```

### [Annotate](https://rubygems.org/gems/annotate)
Dokumentiert halbautomatisch Model, Routes, Fixtures usw. Über den Befehl `annotate -r` werden eure Routen in der `routes.rb` abgebildet und mit `annotate -i -k` werden die Attribute inkl. Fremdschlüsselbeziehungen in euren Model Dateien abgebildet.

```ruby
# Gemfile
group :development do
  gem 'annotate'
end
```

### [Bullet](https://rubygems.org/gems/bullet)
Hilft bei der Erkennung von N+1 Problemen, schmeißt je nach Konfiguration Warnungen in der Console und/oder im Log
```ruby
# Gemfile
group :development do
  gem 'bullet'
end
```
Man kann noch in viel mehr Channels loggen, mir reichen jedoch die Console und ein kleiner Hinweis beim Seitenaufruf
```ruby
# config/environments/development.rb

config.after_initialize do
  Bullet.enable = true
  Bullet.console = true
  Bullet.add_footer = true
  Bullet.bullet_logger = true
end
```


### [Oink](https://rubygems.org/gems/oink)
Ein Logparser, um Speicherlecks besser zu erkennen
```ruby
# Gemfile
group :development do
  gem 'oink'
end
```
Oink muss noch initalisiert werden, dafür legt ihr unter `config/initalizers` einfach so eine Datei ab.
```ruby
# config/initalizers/oink.rb
Rails.application.middleware.use Oink::Middleware if Rails.env.development?
```


## Um das kopieren zu vereinfachen

```ruby
# Gemfile
group :development do
  gem 'annotate'
  gem 'bullet'
  gem 'oink'
  gem 'rubocop', require: false

  # Standard Rails Gems
  gem 'listen', '>= 3.0.5', '< 3.2'
  gem 'web-console', '>= 3.3.0'
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
end
```
